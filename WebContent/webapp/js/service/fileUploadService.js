root.service('fileUploadService', ['$http', '$q', function ($http, $q) {
    
	this.uploadFile = function(file, name){
		
        var fileFormData = new FormData();
        fileFormData.append('file', file);
        fileFormData.append('name', name);
        
        var deffered = $q.defer();
        $http.post('rest/uploadController/upload/', fileFormData, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
        }).success(function (response) {
            deffered.resolve(response);

        }).error(function (response) {
            deffered.reject(response);
        });

        return deffered.promise;
    }
	
	this.getFiles = function(){
		$http.get('rest/uploadController/getFiles/', {
        }).success(function (response) {
            deffered.resolve(response);

        }).error(function (response) {
            deffered.reject(response);
        });
	}
}]);
