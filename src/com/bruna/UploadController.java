package com.bruna;

import java.io.InputStream;
import java.util.Date;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;

import com.bruna.model.FileUploaded;
import com.bruna.service.FileService;
import com.sun.jersey.multipart.FormDataParam;

import io.swagger.annotations.Api;

@Api(value = "upload", description = " Connection between the front and the back and, control the requests") 
@Path("/uploadController")
public class UploadController {
	
	@Autowired
	FileService fileService ;
	
	@POST
	@Path("/upload")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	@Produces(MediaType.APPLICATION_JSON)
	public FileUploaded uploadFile(@FormDataParam("file") InputStream uploadedInputStream,
			@FormDataParam("name") String name) {

		FileUploaded file = new FileUploaded();
		file.setName(name);
		file.setCodUser(1L); //Mock codeUser;
		file.setHoraEnvio(new Date());
		file.setFile(uploadedInputStream);

		return fileService.addFile(file);
	}
	
	@GET
	@Path("/getFiles")
	@Produces(MediaType.APPLICATION_JSON)
	public List<FileUploaded> getFiles() {
		return fileService.getFiles();
	}

}
