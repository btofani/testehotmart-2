package com.bruna.dao;

import java.util.List;

import com.bruna.model.FileUploaded;

public interface FileDao {
	
	public void addFile(FileUploaded file);
	
	public List<FileUploaded> getFiles();
	
	public Long getLastIndex();
}
