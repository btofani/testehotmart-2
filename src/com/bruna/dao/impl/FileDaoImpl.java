package com.bruna.dao.impl;

import java.util.ArrayList;
import java.util.List;

import com.bruna.dao.FileDao;
import com.bruna.model.FileUploaded;

public class FileDaoImpl implements FileDao {

	//Mock BD
	private List<FileUploaded> listBd = new ArrayList<FileUploaded>();
	
	public void addFile(FileUploaded file) {
		listBd.add(file);
	}

	public List<FileUploaded> getFiles() {
		return listBd;
	}
	
	public Long getLastIndex() {
		return listBd.get(listBd.size()).getId();
	}
}
