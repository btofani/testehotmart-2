package com.bruna.model;

import java.io.InputStream;
import java.util.Date;

import org.springframework.web.multipart.MultipartFile;

public class FileUploaded {
	
	private Long id;
	private Long codUser;
	private String name;
	private Date horaEnvio;
	private Long bloco;
	private String path;
	private InputStream file;
	private MultipartFile multiFile;
	
	public MultipartFile getMultiFile() {
		return multiFile;
	}
	public void setMultiFile(MultipartFile multiFile) {
		this.multiFile = multiFile;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	public InputStream getFile() {
		return file;
	}
	public void setFile(InputStream file) {
		this.file = file;
	}
	public Long getCodUser() {
		return codUser;
	}
	public void setCodUser(Long codUser) {
		this.codUser = codUser;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Date getHoraEnvio() {
		return horaEnvio;
	}
	public void setHoraEnvio(Date horaEnvio) {
		this.horaEnvio = horaEnvio;
	}
	public Long getBloco() {
		return bloco;
	}
	public void setBloco(Long bloco) {
		this.bloco = bloco;
	}
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
}
