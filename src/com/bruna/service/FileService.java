package com.bruna.service;

import java.util.List;

import com.bruna.model.FileUploaded;

public interface FileService {
	
	public FileUploaded addFile(FileUploaded file);
	
	public List<FileUploaded> getFiles();
}
