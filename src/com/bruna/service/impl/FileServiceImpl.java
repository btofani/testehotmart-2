package com.bruna.service.impl;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

import javax.servlet.MultipartConfigElement;
import javax.servlet.ServletRegistration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.multipart.MultipartFile;

import com.bruna.dao.FileDao;
import com.bruna.model.FileUploaded;
import com.bruna.service.FileService;

public class FileServiceImpl implements FileService {

	@Autowired
	FileDao fileDao;
	
	private static final String DIRECTORY = "directory/";
	private static final long MAX_FILE_SIZE = 1024 * 1024 * 1;
	private static final long MAX_REQUEST_SIZE = 1024 * 1024 * 5;
	private static final int FILE_SIZE_THRESHOLD = 0;

	public FileUploaded addFile(FileUploaded file) {

		file.setId(fileDao.getLastIndex() + 1);// Mock @GenerateId;
		file.setPath(DIRECTORY);
		
		MultipartFile multipartFile = (MultipartFile) file.getFile();
		file.setMultiFile(multipartFile);
		
		fileDao.addFile(file);
		this.writeToFile(file.getFile(), file.getName());

		return file;
	}

	public List<FileUploaded> getFiles() {
		return fileDao.getFiles();
	}

	private void customizeRegistration(ServletRegistration.Dynamic registration) {
        registration.setMultipartConfig(getMultipartConfigElement());
    }
	
	private MultipartConfigElement getMultipartConfigElement() {
		MultipartConfigElement multipartConfigElement = new MultipartConfigElement(DIRECTORY, MAX_FILE_SIZE,
				MAX_REQUEST_SIZE, FILE_SIZE_THRESHOLD);
		return multipartConfigElement;
	}

	private void writeToFile(InputStream uploadedInputStream, String name) {

		try {
			OutputStream out = new FileOutputStream(new File(DIRECTORY + name));
			int read = 0;
			byte[] bytes = new byte[1024];

			out = new FileOutputStream(new File(DIRECTORY + name));
			while ((read = uploadedInputStream.read(bytes)) != -1) {
				out.write(bytes, 0, read);
			}
			out.flush();
			out.close();
		} catch (IOException e) {

			e.printStackTrace();
		}

	}

}
